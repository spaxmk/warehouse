namespace Warehouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateModels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employees", "Warehouse_Id", "dbo.Warehouses");
            DropIndex("dbo.Employees", new[] { "Warehouse_Id" });
            DropColumn("dbo.Employees", "Warehouse_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employees", "Warehouse_Id", c => c.Int());
            CreateIndex("dbo.Employees", "Warehouse_Id");
            AddForeignKey("dbo.Employees", "Warehouse_Id", "dbo.Warehouses", "Id");
        }
    }
}
