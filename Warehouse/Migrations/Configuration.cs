namespace Warehouse.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Warehouse.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            
            context.warehouses.AddOrUpdate
                (
                new Warehouse() { Id = 1, Name = "Magacine", Adress = "Ilije Bircanina 3", City= "Novi Sad", Contact = "0659876532", Surface = 240d }    
                );

            context.SaveChanges();
            //context.assigments.AddOrUpdate(
            //    new Assigment() { Id = 1, Name = "Assigment1", Description = "Desc", EmployeeId = 1, IsCompleted = false },
            //    new Assigment() { Id = 1, Name = "Assigment2", Description = "Desc", EmployeeId = 2, IsCompleted = false }
            //    );
            //context.SaveChanges();
            //context.employees.AddOrUpdate(
            //    new Employee() { Id = 1, FirstName = "Spasoja", LastName = "Stojsic", Adress = "Bulevar Oslobodjenja 11", City = "Novi Sad", PhoneNumber = "0655499117" },
            //    new Employee() { Id = 1, FirstName = "Nikola", LastName = "Nikolic", Adress = "Bulevar Partijaha Pavla 101", City = "Novi Sad", PhoneNumber = "0612345678" }
            //    );
            //context.SaveChanges();
        }
    }
}
