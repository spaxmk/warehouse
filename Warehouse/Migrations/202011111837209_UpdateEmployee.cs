namespace Warehouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateEmployee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "LicenceNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "LicenceNumber");
        }
    }
}
