namespace Warehouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAssigmentModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Assigments", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Assigments", new[] { "EmployeeId" });
            AddColumn("dbo.Assigments", "Completed", c => c.String(nullable: false));
            AddColumn("dbo.Assigments", "AccountEmail", c => c.String(nullable: false));
            DropColumn("dbo.Assigments", "IsCompleted");
            DropColumn("dbo.Assigments", "EmployeeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Assigments", "EmployeeId", c => c.Int(nullable: false));
            AddColumn("dbo.Assigments", "IsCompleted", c => c.Boolean(nullable: false));
            DropColumn("dbo.Assigments", "AccountEmail");
            DropColumn("dbo.Assigments", "Completed");
            CreateIndex("dbo.Assigments", "EmployeeId");
            AddForeignKey("dbo.Assigments", "EmployeeId", "dbo.Employees", "Id", cascadeDelete: true);
        }
    }
}
