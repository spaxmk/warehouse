namespace Warehouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updatemodelassigment : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Assigments", "Description", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Assigments", "Description", c => c.String(nullable: false, maxLength: 255));
        }
    }
}
