﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        [StringLength(50)]
        public string Adress { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }

        public string LicenceNumber { get; set; }

        //public List<Assigment> Assigments { get; set; }
    }
}