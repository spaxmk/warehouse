﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Warehouse.Models
{
    public class Assigment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(25)]
        public string Name { get; set; }
        [Required]
        //[StringLength(255)]
        public string Description { get; set; }
        [Required]
        public string Completed { get; set; }
        [Required]
        public string AccountEmail { get; set; }


        //[ForeignKey("Id")]
        //public int AccountId { get; set; }
        //public RegisterBindingModel EmployeeEmail { get; set; }
    }
}