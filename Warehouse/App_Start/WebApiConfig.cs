﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Unity;
using Unity.Lifetime;
using Warehouse.Repository;
using Warehouse.Repository.Interfaces;
using Warehouse.Resolver;

namespace Warehouse
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Unity
            var container = new UnityContainer();
            container.RegisterType<IWarehouseRepository, WarehouseRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IAssigmentRepository, AssigmentRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IEmployeeRepository, EmployeeRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            //var container2 = new UnityContainer();
            //container2.RegisterType<IAssigmentRepository, AssigmentRepository>(new HierarchicalLifetimeManager());
            //config.DependencyResolver = new UnityResolver(container2);

            //var container3 = new UnityContainer();
            //container3.RegisterType<IEmployeeRepository, EmployeeRepository>(new HierarchicalLifetimeManager());
            //config.DependencyResolver = new UnityResolver(container3);

        }
    }
}
