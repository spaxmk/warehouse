﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Web.Http;
using Warehouse.Repository.Interfaces;

namespace Warehouse.Controllers
{
    [Authorize(Roles = "admin")]
    public class WarehousesController : ApiController
    {
        IWarehouseRepository _repository { get; set; }

        public WarehousesController(IWarehouseRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IEnumerable<Models.Warehouse> GetAll()
        {
            return _repository.GetAll();
        }

        [HttpGet]
        public IHttpActionResult GetById(int? id)
        {
            if (id == null)
                return BadRequest();
            var warehouse = _repository.GetById(id);
            if (warehouse == null)
                return NotFound();

            return Ok(warehouse);
        }

        [HttpPost]
        public IHttpActionResult PostWarehouse(Models.Warehouse warehouse)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(warehouse);

            return CreatedAtRoute("DefaultApi", new { id = warehouse.Id }, warehouse);
        }

        [HttpPut]
        public IHttpActionResult PutWarehouse(Models.Warehouse warehouse, int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(id!= warehouse.Id)
            {
                return NotFound();
            }
            try
            {
                _repository.Update(warehouse);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(warehouse);
        }

        public IHttpActionResult Delete(int? id)
        {
            var warehouse = _repository.GetById(id);
            if (warehouse == null)
            {
                return NotFound();
            }
            _repository.Delete(warehouse);
            return Ok();
        }

    }
}
