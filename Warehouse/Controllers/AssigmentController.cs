﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Web.Http;
using System.Web.Http.Description;
using Warehouse.Models;
using Warehouse.Repository.Interfaces;

namespace Warehouse.Controllers
{
    [Authorize(Roles = "admin,user")]
    public class AssigmentController : ApiController
    {
        IAssigmentRepository _repository { get; set; }

        public AssigmentController(IAssigmentRepository repository)
        {
            _repository = repository;
        }

        [Authorize(Roles = "admin,user")]
        [HttpGet]
        [ResponseType(typeof(Models.Warehouse))]
        public IEnumerable<Assigment> GetAll()
        {
            return _repository.GetAll();
        }

        [Authorize(Roles = "user,admin")]
        [HttpGet]
        [ResponseType(typeof(Models.Warehouse))]
        public IHttpActionResult GetById(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var assigment = _repository.GetById(id);
            if(assigment == null)
            {
                return NotFound();
            }
            return Ok(assigment);
        }

        [Authorize(Roles = "admin,user")]
        [HttpGet]
        public IEnumerable<Assigment> GetByAccountEmail(string email)
        {
            return _repository.GetByAccountEmail(email);
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ResponseType(typeof(Models.Warehouse))]
        public IHttpActionResult PostAssigment(Assigment assigment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(assigment);
            return Ok(assigment);
        }

        [Authorize(Roles = "admin,user")]
        [HttpPut]
        [ResponseType(typeof(Models.Warehouse))]
        public IHttpActionResult PutAssigment(Assigment assigment, int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (id != assigment.Id)
            {
                return NotFound();
            }
            try
            {
                _repository.Update(assigment);
            }
            catch 
            {
                return BadRequest();
                throw;
            }
            return Ok(assigment);

        }

        [Authorize(Roles = "admin")]
        [ResponseType(typeof(Models.Warehouse))]
        public IHttpActionResult Delete(int? id)
        {
            if(id == null)
            {
                return BadRequest();
            }
            var assigment = _repository.GetById(id);
            if (assigment == null)
            {
                return NotFound();
            }
            _repository.Delete(assigment);
            return Ok();
        }
    }
}
