﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Warehouse.Models;
using Warehouse.Repository;

namespace Warehouse.Controllers
{
    public class EmployeeController : ApiController
    {
        private EmployeeRepository _repository { get; set; }

        public EmployeeController(EmployeeRepository repository)
        {
            _repository = repository;
        }

        public IHttpActionResult PostEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            _repository.Add(employee);

            return CreatedAtRoute("DefaultApi", new { id = employee.Id }, employee);
        }

        public IHttpActionResult PostLicence(string licence)
        {
            if (licence == null)
                return BadRequest();
            
            var employee = _repository.GetByLicence(licence);
            if (employee == null)
                return NotFound();
            return Ok(employee);
        }

    }
}
