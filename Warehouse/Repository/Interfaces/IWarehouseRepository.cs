﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Models;

namespace Warehouse.Repository.Interfaces
{
    public interface IWarehouseRepository
    {
        IEnumerable<Models.Warehouse> GetAll();
        Models.Warehouse GetById(int? id);
        void Add(Models.Warehouse warehouse);
        void Update(Models.Warehouse warehouse);
        void Delete(Models.Warehouse warehouse);

    }
}
