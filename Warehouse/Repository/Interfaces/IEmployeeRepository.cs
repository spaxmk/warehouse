﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Models;

namespace Warehouse.Repository.Interfaces
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetAll();
        Employee GetById(int? id);
        Employee GetByLicence(string licence);
        void Add(Employee employee);
        void Update(Employee employee);
        void Delete(Employee employee);
    }
}
