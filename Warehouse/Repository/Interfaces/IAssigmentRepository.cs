﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Models;

namespace Warehouse.Repository.Interfaces
{
    public interface IAssigmentRepository
    {
        IEnumerable<Assigment> GetAll();
        Assigment GetById(int? id);
        IEnumerable<Assigment> GetByAccountEmail(string email);
        void Add(Assigment assigment);
        void Update(Assigment assigment);
        void Delete(Assigment assigment);
    }
}
