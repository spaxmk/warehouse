﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Repository.Interfaces;
using Warehouse.Models;
using System.Data;

namespace Warehouse.Repository
{
    public class EmployeeRepository : IDisposable, IEmployeeRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Employee> GetAll()
        {
            return db.employees; 
        }

        public Employee GetById(int? id)
        {
            return db.employees.FirstOrDefault(p => p.Id == id);
        }

        public void Add(Employee employee)
        {
            db.employees.Add(employee);
            db.SaveChanges();
        }

        public void Update(Employee employee)
        {
            db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DBConcurrencyException)
            {

                throw;
            }
        }

        public void Delete(Employee employee)
        {
            db.employees.Remove(employee);
            db.SaveChanges();
        }

        public Employee GetByLicence(string licence)
        {
            return db.employees.FirstOrDefault(p => p.LicenceNumber == licence);
        }
    }
}