﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Warehouse.Models;
using Warehouse.Repository.Interfaces;

namespace Warehouse.Repository
{
    public class WarehouseRepository : IWarehouseRepository, IDisposable
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        public void Add(Models.Warehouse warehouse)
        {
            db.warehouses.Add(warehouse);
            db.SaveChanges();
        }

        public void Delete(Models.Warehouse warehouse)
        {
            db.warehouses.Remove(warehouse);
            db.SaveChanges();
        }

        public IEnumerable<Models.Warehouse> GetAll()
        {
            return db.warehouses;
        }

        public Models.Warehouse GetById(int? id)
        {
            return db.warehouses.FirstOrDefault(p => p.Id == id);
        }

        public void Update(Models.Warehouse warehouse)
        {
            db.Entry(warehouse).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DBConcurrencyException)
            {

                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        
    }
}