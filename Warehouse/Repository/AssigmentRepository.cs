﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dispatcher;
using Warehouse.Repository.Interfaces;
using Warehouse.Models;
using System.Data;

namespace Warehouse.Repository
{
    public class AssigmentRepository : IDisposable, IAssigmentRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Assigment assigment)
        {
            db.assigments.Add(assigment);
            db.SaveChanges();
        }

        public void Delete(Assigment assigment)
        {
            db.assigments.Remove(assigment);
            db.SaveChanges();
        }

        public IEnumerable<Assigment> GetAll()
        {
            return db.assigments;
        }

        public Assigment GetById(int? id)
        {
            return db.assigments.FirstOrDefault(p=>p.Id == id);
        }

        public IEnumerable<Assigment> GetByAccountEmail(string email)
        {
            IEnumerable<Assigment> assigments = from x in db.assigments where x.AccountEmail == email select x;
            return assigments;
        }

        public void Update(Assigment assigment)
        {
            db.Entry(assigment).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DBConcurrencyException)
            {

                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        
    }
}